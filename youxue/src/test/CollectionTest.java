import com.alibaba.fastjson.JSONObject;
import com.youxue.service.CollectionService;
import com.youxue.service.CommentService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import javax.servlet.http.HttpServlet;

/**
 * Created by 15c on 2018/5/21.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml", "classpath:spring-mybatis.xml"})
public class CollectionTest {
    private static final Logger LOGGER = Logger.getLogger(CollectionTest.class);
    @Autowired
    CollectionService collectionService;

    @Test
    public void testAdd() {
        HttpServlet httpServlet=new HttpRequestHandlerServlet();

//        Collection collection = new Collection();
//        collection.setCollUserid(1);
//        collection.setCollDynaid(1);
//        int result = collectionService.addACollection(collection);
//        System.out.println(">>>>>>>>>>>>>>>" + result);
        System.out.println(httpServlet.getServletContext().getRealPath("/resources/img"));
    }

    @Test
    public void testReduce(){
        int result = collectionService.reduceACollection(3);
        System.out.println(">>>>>>>>>>>>>>>" + result);
    }
    @Test
    public void showAllCollectionByPage(){

        System.out.println(">>>>>>>>>>>>>>>" + JSONObject.toJSONString(collectionService.showAllCollectionByPage(1)));
    }
    @Autowired
    CommentService commentService;
    @Test
    public void showCommentAndUserInfo(){

        System.out.println(">>>>>>>>>>>>>>>" + JSONObject.toJSONString(commentService.showCommentAndUserInfo(6)));
    }


}
