import com.alibaba.fastjson.JSONObject;
import com.youxue.service.UserObjectService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * Created by 15c on 2018/1/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml", "classpath:spring-mybatis.xml"})
public class UserTest {
    private static final Logger LOGGER = Logger.getLogger(UserTest.class);
    @Autowired
    UserObjectService userService;

    @Test
    public void testShowToursByMyAndOther(){

        System.out.println(">>>>>>>>>>>>>>>" + JSONObject.toJSONString(userService.showToursByMyAndOther(1)));
    }


}
