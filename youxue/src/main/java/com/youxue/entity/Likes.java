package com.youxue.entity;

import java.util.Date;

public class Likes extends LikesKey {
    private Date likeDate;

    private TourDynamics tourDynamics;

    public TourDynamics getTourDynamics() {
        return tourDynamics;
    }

    public void setTourDynamics(TourDynamics tourDynamics) {
        this.tourDynamics = tourDynamics;
    }

    public Date getLikeDate() {
        return likeDate;
    }

    public void setLikeDate(Date likeDate) {
        this.likeDate = likeDate;
    }
}