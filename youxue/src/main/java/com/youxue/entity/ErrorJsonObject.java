package com.youxue.entity;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by 15c on 2017/11/16.
 */
public class ErrorJsonObject extends JSONObject {
    public ErrorJsonObject(Integer error,String msg) {
        this.put("error", error);
        this.put("msg", msg);
    }
}
