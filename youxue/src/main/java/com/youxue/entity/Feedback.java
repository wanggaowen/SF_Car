package com.youxue.entity;

import java.util.Date;

public class Feedback {
    private Integer feedId;

    private Integer feedUserid;

    private String feedContent;

    private Date feedTime;

    private Integer fparentId;

    public Integer getFeedId() {
        return feedId;
    }

    public void setFeedId(Integer feedId) {
        this.feedId = feedId;
    }

    public Integer getFeedUserid() {
        return feedUserid;
    }

    public void setFeedUserid(Integer feedUserid) {
        this.feedUserid = feedUserid;
    }

    public String getFeedContent() {
        return feedContent;
    }

    public void setFeedContent(String feedContent) {
        this.feedContent = feedContent == null ? null : feedContent.trim();
    }

    public Date getFeedTime() {
        return feedTime;
    }

    public void setFeedTime(Date feedTime) {
        this.feedTime = feedTime;
    }

    public Integer getFparentId() {
        return fparentId;
    }

    public void setFparentId(Integer fparentId) {
        this.fparentId = fparentId;
    }
}