package com.youxue.entity;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by 15c on 2017/11/16.
 */
public class CorrectJsonObject extends JSONObject {
    public CorrectJsonObject(String msg) {
        this.put("error", 0);
        this.put("msg", msg);
    }

    public void setData(Object data){
        this.put("data",data);
    }
}
