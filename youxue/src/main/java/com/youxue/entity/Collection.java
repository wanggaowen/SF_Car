package com.youxue.entity;

import java.util.Date;

public class Collection {
    private Integer collId;

    private Integer collUserid;

    private Integer collDynaid;

    private Date collDate;

    private TourDynamics tourDynamics;

    public TourDynamics getTourDynamics() {
        return tourDynamics;
    }

    public void setTourDynamics(TourDynamics tourDynamics) {
        this.tourDynamics = tourDynamics;
    }

    public Integer getCollId() {
        return collId;
    }

    public void setCollId(Integer collId) {
        this.collId = collId;
    }

    public Integer getCollUserid() {
        return collUserid;
    }

    public void setCollUserid(Integer collUserid) {
        this.collUserid = collUserid;
    }

    public Integer getCollDynaid() {
        return collDynaid;
    }

    public void setCollDynaid(Integer collDynaid) {
        this.collDynaid = collDynaid;
    }

    public Date getCollDate() {
        return collDate;
    }

    public void setCollDate(Date collDate) {
        this.collDate = collDate;
    }
}