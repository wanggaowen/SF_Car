package com.youxue.entity;

import java.util.Date;

public class Follow {
    private Integer followId;

    private Integer followfUserid;

    private Integer followtUserid;

    private Date followDate;

    private UserObject user;

    public UserObject getUser() {
        return user;
    }

    public void setUser(UserObject user) {
        this.user = user;
    }

    public Integer getFollowId() {
        return followId;
    }

    public void setFollowId(Integer followId) {
        this.followId = followId;
    }

    public Integer getFollowfUserid() {
        return followfUserid;
    }

    public void setFollowfUserid(Integer followfUserid) {
        this.followfUserid = followfUserid;
    }

    public Integer getFollowtUserid() {
        return followtUserid;
    }

    public void setFollowtUserid(Integer followtUserid) {
        this.followtUserid = followtUserid;
    }

    public Date getFollowDate() {
        return followDate;
    }

    public void setFollowDate(Date followDate) {
        this.followDate = followDate;
    }
}