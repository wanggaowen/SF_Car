package com.youxue.entity;

public class LikesKey {
    private Integer likedynaId;

    private Integer likeuserId;

    public Integer getLikedynaId() {
        return likedynaId;
    }

    public void setLikedynaId(Integer likedynaId) {
        this.likedynaId = likedynaId;
    }

    public Integer getLikeuserId() {
        return likeuserId;
    }

    public void setLikeuserId(Integer likeuserId) {
        this.likeuserId = likeuserId;
    }
}