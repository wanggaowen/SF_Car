package com.youxue.entity;

import java.util.Date;

public class Comment {
    private Integer commentId;

    private Integer commentUserid;

    private Integer tourdynamicsId;

    private Integer replayUserid;

    private Date commentDate;

    private Integer commentIsread;

    private String commentImg;

    private Integer parentId;

    private String commentContent;

    //动态信息对象
    private TourDynamics tourDynamics;

    public TourDynamics getTourDynamics() {
        return tourDynamics;
    }

    public void setTourDynamics(TourDynamics tourDynamics) {
        this.tourDynamics = tourDynamics;
    }

    public Integer getReplyNum() {
        return replyNum;
    }

    public void setReplyNum(Integer replyNum) {
        this.replyNum = replyNum;
    }

    //直接评论回复数
    private Integer replyNum;

    //回复对象
    private Comment comment;

    //评论对象
    private Comment comment1;

    //评论者
    private UserObject user1;

    //回复者
    private UserObject user;

    //被回复者
    private UserObject user2;

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Comment getComment1() {
        return comment1;
    }

    public void setComment1(Comment comment1) {
        this.comment1 = comment1;
    }


    public UserObject getUser() {
        return user;
    }

    public void setUser(UserObject user) {
        this.user = user;
    }

    public UserObject getUser2() {
        return user2;
    }

    public void setUser2(UserObject user2) {
        this.user2 = user2;
    }


    public UserObject getUser1() {
        return user1;
    }


    public void setUser1(UserObject user1) {
        this.user1 = user1;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getCommentUserid() {
        return commentUserid;
    }

    public void setCommentUserid(Integer commentUserid) {
        this.commentUserid = commentUserid;
    }

    public Integer getTourdynamicsId() {
        return tourdynamicsId;
    }

    public void setTourdynamicsId(Integer tourdynamicsId) {
        this.tourdynamicsId = tourdynamicsId;
    }

    public Integer getReplayUserid() {
        return replayUserid;
    }

    public void setReplayUserid(Integer replayUserid) {
        this.replayUserid = replayUserid;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Integer getCommentIsread() {
        return commentIsread;
    }

    public void setCommentIsread(Integer commentIsread) {
        this.commentIsread = commentIsread;
    }

    public String getCommentImg() {
        return commentImg;
    }

    public void setCommentImg(String commentImg) {
        this.commentImg = commentImg == null ? null : commentImg.trim();
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent == null ? null : commentContent.trim();
    }
}