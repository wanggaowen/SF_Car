package com.youxue.entity;

import java.util.Date;
import java.util.List;

public class TourDynamics {
    private Integer tourdynaId;

    private Integer tourdynaUserId;

    private String tourdynaTitle;

    private String tourdynaContent;

    private Date tourdynaDate;

    private Integer tourdynaLikeNum;

    private String tourdynaImg;

    private Date tourdynaEndTime;

    private Date tourdynaOutTime;

    private String placeFrom;

    private String placeTo;

    private Integer tourdynaType;

    private String contactway;

    private Integer tourdynaCommentNum;
    //位置经纬度
    private Double positionX;

    private Double positionY;

    public Double getPositionX() {
        return positionX;
    }

    public void setPositionX(Double positionX) {
        this.positionX = positionX;
    }

    public Double getPositionY() {
        return positionY;
    }

    public void setPositionY(Double positionY) {
        this.positionY = positionY;
    }

    //动态评价列表
    private List<Comment> commentList;

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    //动态发布者
    private UserObject user;

    public UserObject getUser() {
        return user;
    }

    public void setUser(UserObject user) {
        this.user = user;
    }

    public Integer getTourdynaId() {
        return tourdynaId;
    }

    public void setTourdynaId(Integer tourdynaId) {
        this.tourdynaId = tourdynaId;
    }

    public Integer getTourdynaUserId() {
        return tourdynaUserId;
    }

    public void setTourdynaUserId(Integer tourdynaUserId) {
        this.tourdynaUserId = tourdynaUserId;
    }

    public String getTourdynaTitle() {
        return tourdynaTitle;
    }

    public void setTourdynaTitle(String tourdynaTitle) {
        this.tourdynaTitle = tourdynaTitle == null ? null : tourdynaTitle.trim();
    }

    public String getTourdynaContent() {
        return tourdynaContent;
    }

    public void setTourdynaContent(String tourdynaContent) {
        this.tourdynaContent = tourdynaContent == null ? null : tourdynaContent.trim();
    }

    public Date getTourdynaDate() {
        return tourdynaDate;
    }

    public void setTourdynaDate(Date tourdynaDate) {
        this.tourdynaDate = tourdynaDate;
    }

    public Integer getTourdynaLikeNum() {
        return tourdynaLikeNum;
    }

    public void setTourdynaLikeNum(Integer tourdynaLikeNum) {
        this.tourdynaLikeNum = tourdynaLikeNum;
    }

    public String getTourdynaImg() {
        return tourdynaImg;
    }

    public void setTourdynaImg(String tourdynaImg) {
        this.tourdynaImg = tourdynaImg == null ? null : tourdynaImg.trim();
    }

    public Date getTourdynaEndTime() {
        return tourdynaEndTime;
    }

    public void setTourdynaEndTime(Date tourdynaEndTime) {
        this.tourdynaEndTime = tourdynaEndTime;
    }

    public Date getTourdynaOutTime() {
        return tourdynaOutTime;
    }

    public void setTourdynaOutTime(Date tourdynaOutTime) {
        this.tourdynaOutTime = tourdynaOutTime;
    }

    public String getPlaceFrom() {
        return placeFrom;
    }

    public void setPlaceFrom(String placeFrom) {
        this.placeFrom = placeFrom == null ? null : placeFrom.trim();
    }

    public String getPlaceTo() {
        return placeTo;
    }

    public void setPlaceTo(String placeTo) {
        this.placeTo = placeTo == null ? null : placeTo.trim();
    }

    public Integer getTourdynaType() {
        return tourdynaType;
    }

    public void setTourdynaType(Integer tourdynaType) {
        this.tourdynaType = tourdynaType;
    }

    public String getContactway() {
        return contactway;
    }

    public void setContactway(String contactway) {
        this.contactway = contactway == null ? null : contactway.trim();
    }

    public Integer getTourdynaCommentNum() {
        return tourdynaCommentNum;
    }

    public void setTourdynaCommentNum(Integer tourdynaCommentNum) {
        this.tourdynaCommentNum = tourdynaCommentNum;
    }
}