package com.youxue.entity;

import com.cangshi.permission.entity.AuthorizationEntity;

/**
 * Created by 15c on 2018/1/16.
 */
public class UserEntity extends AuthorizationEntity {

    private Integer permissionKey;

    private Integer userId;

    private Integer userStatus;

    private String username;

    public UserEntity() {
    }

    public UserEntity(Integer permissionKey, Integer userId, Integer userStatus, String username) {
        this.permissionKey = permissionKey;
        this.userId = userId;
        this.userStatus = userStatus;
        this.username = username;
    }

    public Integer getPermissionKey() {
        return this.permissionKey;
    }

    public void setPermissionKey(Integer permissionKey) {
        this.permissionKey = permissionKey;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
