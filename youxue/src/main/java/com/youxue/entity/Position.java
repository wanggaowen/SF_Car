package com.youxue.entity;

public class Position {
    private Integer tourpoId;

    private Double positionX;

    private Double positionY;

    public Integer getTourpoId() {
        return tourpoId;
    }

    public void setTourpoId(Integer tourpoId) {
        this.tourpoId = tourpoId;
    }

    public Double getPositionX() {
        return positionX;
    }

    public void setPositionX(Double positionX) {
        this.positionX = positionX;
    }

    public Double getPositionY() {
        return positionY;
    }

    public void setPositionY(Double positionY) {
        this.positionY = positionY;
    }
}