package com.youxue.entity;

import com.cangshi.permission.entity.AuthorizationEntity;
import com.cangshi.permission.entity.ObjectSources;
import com.cangshi.permission.entity.Token;
import com.cangshi.permission.entity.UserSubject;
import com.youxue.service.UserObjectService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by 15c on 2018/1/16.
 */
public class OriginalUser extends ObjectSources {
    @Autowired
    UserObjectService userObjectService;


    protected UserSubject.CheckAndBuild checkAndBuild(final Token token) {
        // 登陆审核是否通过
        Boolean isPass = true;

        // 获取登录信息
        UserObject checkUser = new UserObject();
        checkUser.setUserEmail(token.getUsername());
        checkUser.setUserTelphone(token.getUsername());
        checkUser.setUserPassword(token.getPassword());
        // 检查登录信息是否存在
        UserObject resultUser = userObjectService.selectUserIdByTelphone(checkUser);
        if (resultUser == null) {
            resultUser = userObjectService.selectUserIdByuserEmail(checkUser);
            if (resultUser == null) {
                isPass = false;
            }
        }
        if (resultUser == null) {

                isPass = false;

        }

        UserObject detailUser = null;

        if (isPass) {

            // 获取预登陆的用户实体
            detailUser = userObjectService.selectAllUserInfoByUserId(resultUser.getUserId());
        }

        // 检查密码是否正确
        if (isPass && !checkUser.getUserPassword().equals(detailUser.getUserPassword())) {

            isPass = false;
        }
        // 检查账户是否被冻结
        if (isPass && !detailUser.getUserState().equals(0)) {

            isPass = false;
        }



        // 声明最终登录审核是否通过
        final Boolean finalIsPass = isPass;
        // 声明最终在buildEntity中用到的用户对象
        final UserObject finalDetailUser = detailUser;
        return new UserSubject.CheckAndBuild() {
            // 该方法返回成功则表明登录成功，否则反之
            protected Boolean setCheckResult() {
                return finalIsPass;
            }
            // 该方法在登录成功后调用，返回一个实体用于生成证书
            protected AuthorizationEntity setBuildResult() {
                if (finalIsPass) {

                    return new UserEntity(
                            finalDetailUser.getUserPower(),
                            finalDetailUser.getUserId(),
                            finalDetailUser.getUserState(),
                            token.getUsername());
                }
                return null;
            }

        };
    }

    protected Boolean checkAuthorizationEntity(AuthorizationEntity authorizationEntity) {
        UserEntity userEntity = (UserEntity) authorizationEntity;
        if (null == userEntity) {
            return false;
        }

        Integer userId = userEntity.getUserId();

        UserObject checkUser = userObjectService.selectAllUserInfoByUserId(userId);

        if (null == checkUser) {
            return false;
        }

        if (!userEntity.getUsername().equals(checkUser.getUserTelphone())) {
            if (!userEntity.getUsername().equals(checkUser.getUserEmail())) {
                return false;
            }
        }
        if (!userEntity.getPermissionKey().equals(checkUser.getUserPower())) {
            return false;
        }

        if (!userEntity.getUserStatus().equals(checkUser.getUserState())) {
            return false;
        }

        return true;
    }
}
