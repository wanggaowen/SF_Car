package com.youxue.util.emailCode;

import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by 15c on 2017/5/10.
 */
public class EmailManage {
    private static String emailAddress;

    public EmailManage(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    //定义集合存放邮箱账号以及对于的验证码
    private static Map<String, EmailUserEntity> mailEntityMap = new HashMap<String, EmailUserEntity>();
    //发送邮件的服务邮箱号
    private static String serverEmailAddress = "1165479047@qq.com";
    //SMTP服务申请的独立密码
    private static String serverEmailCode = "dxeseflxmucqbafi";

    public static String getServerEmailAddress() {
        return serverEmailAddress;
    }

    public static void setServerEmailAddress(String serverEmailAddress) {
        EmailManage.serverEmailAddress = serverEmailAddress;
    }

    public static String getServerEmailCode() {
        return serverEmailCode;
    }

    public static void setServerEmailCode(String serverEmailCode) {
        EmailManage.serverEmailCode = serverEmailCode;
    }

    public void addEmail() {

                //将邮箱号以及生成的验证码放进map集合
                mailEntityMap.put(emailAddress, new EmailUserEntity(emailAddress));
        new Thread(new Runnable() {
            public void run() {
                try {
                    sendEmail(serverEmailAddress, serverEmailCode);
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //获得对应email实体对象
    public static EmailUserEntity getEmail(String emailAddress) {
        return mailEntityMap.get(emailAddress);
    }

    //发送验证码
    private void sendEmail(final String serverEmailAddress, final String serverEmailCode) throws GeneralSecurityException, MessagingException {

        Properties props = new Properties();
        // 开启debug调试
        props.setProperty("mail.debug", "true");
        // 设置邮件服务器主机名
        props.setProperty("mail.host", "smtp.qq.com");
        // 发送服务器需要身份验证
        props.setProperty("mail.smtp.auth", "true");
        // 发送邮件协议名称
        props.setProperty("mail.transport.protocol", "smtp");
        //QQ邮箱的SSL加密。
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.socketFactory", sf);

        //authenticator参数，登录自己的邮箱帐号密码，
        Authenticator authenticator = new Authenticator() {
            /**
             * 注意，QQ邮箱的规则是如果不是由腾讯的网页或者客户端打开登录的话，在其他任何地方
             *登录邮箱，密码必须使用授权码，授权码下面会讲解，vlyvawibbsribgee
             *xxxxxxx:自己的QQ邮箱登录帐号，也就是qq号
             *yyyyyyy:密码，使用授权码登录，而不能使用原始的QQ密码
             */
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(serverEmailAddress, serverEmailCode);
            }
        };
        //创建会话
        Session session = Session.getDefaultInstance(props, authenticator);

        //2、发送的内容对象Message
        Message message = new MimeMessage(session);
        //2.1、发件人是谁
        message.setFrom(new InternetAddress(serverEmailAddress));
        // 2.2  , to:收件人 ; cc:抄送 ; bcc :暗送.
        /**
         * 收件人是谁？
         *         第一个参数：
         *             RecipientType.TO    代表收件人
         *             RecipientType.CC    抄送
         *             RecipientType.BCC    暗送
         *         比如A要给B发邮件，但是A觉得有必要给要让C也看看其内容，就在给B发邮件时，
         *         将邮件内容抄送给C，那么C也能看到其内容了，但是B也能知道A给C抄送过该封邮件
         *         而如果是暗送(密送)给C的话，那么B就不知道A给C发送过该封邮件。
         *     第二个参数
         *         收件人的地址，或者是一个Address[]，用来装抄送或者暗送人的名单。或者用来群发。
         */
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(emailAddress));
        // 2.3 主题（标题）
        message.setSubject("欢迎注册加入我们！");
        // 2.4 正文
        String str = "您的验证码是：" + getEmail(emailAddress).getMailCheckString();
        //设置上下文格式
        message.setContent(str, "text/html;charset=UTF-8");
        //3、发送
        Transport.send(message);

    }

}


