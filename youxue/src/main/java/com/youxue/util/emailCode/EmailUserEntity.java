package com.youxue.util.emailCode;

import java.util.Random;

/**
 * Created by 15c on 2017/5/10.
 */

/**
 * 邮箱用户实体 包括邮箱地址与随机产生的验证码；
 */
public class EmailUserEntity {
    private String mailAddress;
    private String mailCheckString;

    public EmailUserEntity(String mailAddress) {
        this.mailAddress = mailAddress;
        this.mailCheckString=buildCheckNum();
    }
    static String buildCheckNum(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(new Random().nextInt(10));
        }
        return stringBuilder.toString();
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getMailCheckString() {
        return mailCheckString;
    }

    public void setMailCheckString(String mailCheckString) {
        this.mailCheckString = mailCheckString;
    }
}
