package com.youxue.service;

import com.youxue.dao.CommentMapper;
import com.youxue.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 15c on 2018/5/22.
 */
@Service(value = "commentService")
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentMapper commentMapper;

    @Override
    public int countCommentNum(Integer tourdynamicsId) {
        return commentMapper.countCommentNum(tourdynamicsId);
    }


    @Override
    public int addComment(Comment comment) {
        return commentMapper.insertSelective(comment);
    }

    @Override
    public List<Comment> showACommentAllReplys(Integer startPos, Integer pageSize, Integer commentId) {
        return commentMapper.selectACommentAllReplysByPage(startPos,pageSize,commentId);
    }

    @Override
    public int countACommentAllReplysNum(Integer commentId) {
        return commentMapper.selectACommentAllReplysNum(commentId);
    }

    @Override
    public Comment showCommentAndUserInfo(Integer commentId) {
        return commentMapper.selectCommentAndUserInfo(commentId);
    }

    @Override
    public List<Comment> showAUserNoRead(Integer userId) {
        return commentMapper.selectAUserNoRead(userId);
    }

    @Override
    public int setUpYesRead(Integer commentId) {
        return commentMapper.updateYesRead(commentId);
    }

    @Override
    public int deleteComment(Integer tourId) {
        return commentMapper.deleteComment(tourId);
    }
}
