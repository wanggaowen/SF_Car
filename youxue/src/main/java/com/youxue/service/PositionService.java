package com.youxue.service;

import com.youxue.entity.Position;

/**
 * Created by 15c on 2018/5/21.
 */
public interface PositionService {
    /**
     * 添加地址
     * @param position
     * @return
     */
    int addPosition(Position position);

    /**
     * 删除地址
     * @param tourpoId
     * @return
     */
    int  deletePosition(Integer tourpoId);

    Position showPosition(Integer tourdynaId);

}
