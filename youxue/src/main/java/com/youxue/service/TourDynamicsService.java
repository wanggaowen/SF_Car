package com.youxue.service;

import com.youxue.entity.Position;
import com.youxue.entity.TourDynamics;

import java.util.Date;
import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
public interface TourDynamicsService {
    /**
     * 发表动态
     *
     * @param tourDynamics
     * @param position
     * @param userId
     * @return
     */
    int addTourDynamics(TourDynamics tourDynamics, Position position, Integer userId);

    /**
     * 删除动态
     *
     * @param tourdynaId
     * @return
     */
    int reduceTourDynamics(Integer tourdynaId);

    /**
     * 分页展示所有动态
     *
     * @return
     */
    List<TourDynamics> showAllTourDynamics(Integer startPos, Integer pageSize);

    /**
     * 查询所有动态数量
     *
     * @return
     */
    int showAllTourDynamicsNum();

    /**
     * 查询所有关注用户动态
     *
     * @param startPos
     * @param pageSize
     * @return
     */
    List<TourDynamics> showAllTourDynamicsByFollow(Integer startPos, Integer pageSize, Integer userId);

    /**
     * 查询所有关注用户动态数量
     *
     * @return
     */
    int showAllTourDynamicsByFollowNum(Integer userId);


    /**
     * 查询最新的动态（按时间降序排列）
     *
     * @param startPos
     * @param pageSize
     * @return
     */
    List<TourDynamics> showAllTourDynamicsByNew(Integer startPos, Integer pageSize);

    /**
     * 关键字检索动态
     *
     * @param startPos
     * @param pageSize
     * @param keyword
     * @return
     */
    List<TourDynamics> showTourDynamicsBySearch(Integer startPos, Integer pageSize, String keyword,Integer type);

    /**
     * 关键字检索动态数量
     *
     * @param keyword
     * @return
     */
    int showTourDynamicsBySearchNum(String keyword,Integer type);

    /**
     * 点赞
     *
     * @param userId
     * @param tourdynaId
     * @return
     */
    int addLike(Integer userId, Integer tourdynaId);

    /**
     * 取消点赞
     *
     * @param userId
     * @param tourdynaId
     * @return
     */
    int reduceLike(Integer userId, Integer tourdynaId);

    /**
     * 查询自己主页或他人主页（包括个人信息、动态列表信息、关注数、fans数）
     * <p>
     * 没有用
     *
     * @param userId
     * @return
     */
    List<TourDynamics> showToursByMyAndOther(Integer userId);

    /**
     * 查询一个动态下所有评论信息(分页显示)
     * @param tourdynaId
     * @return
     */
    TourDynamics showAToursAllComment(Integer startPos, Integer pageSize,Integer tourdynaId);


    /**
     * 查询一个动态信息<无直接评论>
     * @param tourdynaId
     * @return
     */
    TourDynamics showAToursInfo(Integer tourdynaId);

    /**
     * 添加回复数（直接评论时）
     * @param tourdynaId
     * @return
     */
    int addCommentNum(Integer tourdynaId);

    /**
     * 动态发布成功，返回推荐
     * @param type
     * @param placeTo
     * @param tourdynaOutTime
     * @return
     */
    List<TourDynamics> getTuiJie(Integer type, String placeTo, Date tourdynaOutTime);

    /**
     * 获取出发日期比现在晚的动态
     * @return
     */
    List<TourDynamics> showNearestNews();
}
