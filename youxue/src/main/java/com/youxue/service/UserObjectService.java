package com.youxue.service;

import com.youxue.entity.UserObject;

import java.util.List;

/**
 * Created by 15c on 2018/4/23.
 */
public interface UserObjectService {
    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    int addUser(UserObject user);

    List<UserObject> findAllUser();

    /**
     * 用于用户在忘记密码时后通过telphoneNumber查找user_id
     */
    UserObject selectUserIdByTelphone(UserObject telphone);

    /**
     * 用于用户在忘记密码时后通过userEmail查找user_id
     */
    UserObject selectUserIdByuserEmail(UserObject userEmail);

    /**
     * 通过用户id获取其所有信息
     *
     * @param userId
     * @return
     */
    UserObject selectAllUserInfoByUserId(Integer userId);

    /**
     * 通过邮箱查询用户
     *
     * @param email
     * @return
     */
    UserObject showUserByEmail(String email);

    /**
     * 通过用户phone查找用户id
     *
     * @param phone
     * @return
     */
    UserObject selectUserIdByPhone(UserObject phone);

    /**
     * 用户个人信息展示
     *
     * @param userId
     * @return
     */
    UserObject showUserPartInfo(Integer userId);

    /**
     * 用户修改个人资料
     *
     * @param user
     * @return
     */
    int updateUser(UserObject user);

    /**
     * 通过用户email查找用户id
     *
     * @param email
     * @return
     */
    UserObject selectUserIdByEmail(UserObject email);

    /**
     * 通过邮箱重置密码
     *
     * @param user
     * @return
     */
    int resetPasswordByemail(UserObject user);

    /**
     * 查询自己主页或他人主页（包括个人信息、动态列表信息、关注数、fans数）
     *
     * @param userId
     * @return
     */
    UserObject showToursByMyAndOther(Integer userId);

    Boolean saveVerifyInfo( String eamil,
                            String verify_realName,
                            String verify_id,
                            String id_photo,
                            String living_photo,
                            String driving_license_photo,
                            String license_plate_photo);
}
