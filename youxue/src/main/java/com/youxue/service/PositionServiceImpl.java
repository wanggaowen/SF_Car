package com.youxue.service;

import com.youxue.dao.PositionMapper;
import com.youxue.entity.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 15c on 2018/5/21.
 */
@Service(value = "positionService")
public class PositionServiceImpl implements PositionService{

    @Autowired
    PositionMapper positionMapper;

    @Override
    public int addPosition(Position position) {
        return positionMapper.insertSelective(position);
    }

    @Override
    public int deletePosition(Integer tourpoId) {
        return positionMapper.deleteByPrimaryKey(tourpoId);
    }

    @Override
    public Position showPosition(Integer tourdynaId) {
        return positionMapper.selectByPrimaryKey(tourdynaId);
    }

}
