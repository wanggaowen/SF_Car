package com.youxue.service;

import com.youxue.entity.Follow;

import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
public interface FollowService {
    /**
     * 添加关注
     * @param follow 关注对象
     * @return
     */
    int addFollow(Follow follow);

    /**
     * 查询关注用户
     *
     * @param userIdF
     * @return
     */
    List<Follow> showAllFollowF(Integer userIdF);

    /**
     * 查询粉丝用户
     *
     * @param userIdT
     * @return
     */
    List<Follow> showAllFollowT(Integer userIdT);

    /**
     * 取消关注
     * @param userIdF
     * @param userIdT
     * @return
     */
    int reduceFollow(Integer userIdF, Integer userIdT);

    /**
     * 查询是否已关注
     * @param userIdF
     * @param userIdT
     * @return
     */
    Follow showAFollow(Integer userIdF, Integer userIdT);

}
