package com.youxue.service;

import com.youxue.dao.LikesMapper;
import com.youxue.dao.TourDynamicsMapper;
import com.youxue.entity.Likes;
import com.youxue.entity.LikesKey;
import com.youxue.entity.Position;
import com.youxue.entity.TourDynamics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
@Service(value = "tourDynamicsService")
public class TourDynamicsServiceImpl implements TourDynamicsService {

    @Autowired
    TourDynamicsMapper tourDynamicsMapper;

    @Autowired
    PositionService positionService;

    @Autowired
    LikesMapper likesMapper;


    @Override
    public int addTourDynamics(TourDynamics tourDynamics, Position position, Integer userId) {
        if (tourDynamics.getTourdynaType()==0){
            tourDynamics.setTourdynaTitle("人找车");
        }else {
            tourDynamics.setTourdynaTitle("车找人");
        }
        int tourResult = tourDynamicsMapper.insertSelective(tourDynamics);
        if (1 == tourResult) {
            int tourId = tourDynamicsMapper.selectTourDynamicsId(userId);
            position.setTourpoId(tourId);
            int poResult = positionService.addPosition(position);
            return poResult;
        } else
            return 0;
    }

    @Override
    public int reduceTourDynamics(Integer tourdynaId) {
        return tourDynamicsMapper.deleteByPrimaryKey(tourdynaId);
    }

    @Override
    public List<TourDynamics> showAllTourDynamics(Integer startPos, Integer pageSize) {
        List<TourDynamics> dynamicsList=tourDynamicsMapper.selectAllTourDynamics(startPos, pageSize);
        Position position;
        for (TourDynamics tourDynamics:dynamicsList){
            position=positionService.showPosition(tourDynamics.getTourdynaId());
            tourDynamics.setPositionX(position.getPositionX());
            tourDynamics.setPositionY(position.getPositionY());
        }
        return dynamicsList;
    }

    @Override
    public int showAllTourDynamicsNum() {
        return tourDynamicsMapper.selectAllTourDynamicsNum();
    }

    @Override
    public List<TourDynamics> showAllTourDynamicsByFollow(Integer startPos, Integer pageSize, Integer userId) {
        List<TourDynamics> dynamicsList=tourDynamicsMapper.selectAllTourDynamicsByFollow(startPos, pageSize, userId);
        Position position;
        for (TourDynamics tourDynamics:dynamicsList){
            position=positionService.showPosition(tourDynamics.getTourdynaId());
            tourDynamics.setPositionX(position.getPositionX());
            tourDynamics.setPositionY(position.getPositionY());
        }
        return dynamicsList;
    }

    @Override
    public int showAllTourDynamicsByFollowNum(Integer userId) {
        return tourDynamicsMapper.selectAllTourDynamicsFollowNum(userId);
    }

    @Override
    public List<TourDynamics> showAllTourDynamicsByNew(Integer startPos, Integer pageSize) {
        List<TourDynamics> dynamicsList=tourDynamicsMapper.selectAllTourDynamicsByNew(startPos, pageSize);
        Position position;
        for (TourDynamics tourDynamics:dynamicsList){
            position=positionService.showPosition(tourDynamics.getTourdynaId());
            tourDynamics.setPositionX(position.getPositionX());
            tourDynamics.setPositionY(position.getPositionY());
        }
        return dynamicsList;
    }

    @Override
    public List<TourDynamics> showTourDynamicsBySearch(Integer startPos, Integer pageSize, String keyword,Integer type) {
        if (keyword != null && !"".equals(keyword)) {
            keyword = "%" + keyword + "%";
        }
        List<TourDynamics> dynamicsList=tourDynamicsMapper.selectTourDynamicsBySearch(startPos, pageSize, keyword,type);
        Position position;
        for (TourDynamics tourDynamics:dynamicsList){
            position=positionService.showPosition(tourDynamics.getTourdynaId());
            tourDynamics.setPositionX(position.getPositionX());
            tourDynamics.setPositionY(position.getPositionY());
        }
        return dynamicsList;
    }

    @Override
    public int showTourDynamicsBySearchNum(String keyword,Integer type) {
        if (keyword != null && !"".equals(keyword)) {
            keyword = "%" + keyword + "%";
        }
        return tourDynamicsMapper.selectTourDynamicsBySearchNum(keyword,type);
    }

    @Override
    public int addLike(Integer userId, Integer tourdynaId) {
        Likes likes = new Likes();
        likes.setLikedynaId(tourdynaId);
        likes.setLikeuserId(userId);
        int result = likesMapper.insertSelective(likes);
        if (1 == result) {
            return tourDynamicsMapper.updateLikeNum(tourdynaId);
        } else {
            return 0;
        }

    }

    @Override
    public int reduceLike(Integer userId, Integer tourdynaId) {
        LikesKey likesKey = new LikesKey();
        likesKey.setLikedynaId(tourdynaId);
        likesKey.setLikeuserId(userId);
        tourDynamicsMapper.reduceLikeNum(tourdynaId);
        return likesMapper.deleteByPrimaryKey(likesKey);
    }

    //没用
    @Override
    public List<TourDynamics> showToursByMyAndOther(Integer userId) {
        List<TourDynamics> dynamicsList=tourDynamicsMapper.selectToursByMyAndOther(userId);
        Position position;
        for (TourDynamics tourDynamics:dynamicsList){
            position=positionService.showPosition(tourDynamics.getTourdynaId());
            tourDynamics.setPositionX(position.getPositionX());
            tourDynamics.setPositionY(position.getPositionY());
        }
        return dynamicsList;
    }

    @Override
    public TourDynamics showAToursAllComment(Integer startPos, Integer pageSize,Integer tourdynaId) {
        TourDynamics tourDynamics=tourDynamicsMapper.selectAToursAllComment(startPos,pageSize,tourdynaId);
        Position position=positionService.showPosition(tourdynaId);
        tourDynamics.setPositionX(position.getPositionX());
        tourDynamics.setPositionY(position.getPositionY());
        return tourDynamics;
    }

    @Override
    public TourDynamics showAToursInfo(Integer tourdynaId) {
        TourDynamics tourDynamics=tourDynamicsMapper.selectByPrimaryKey(tourdynaId);
        Position position=positionService.showPosition(tourdynaId);
        tourDynamics.setPositionX(position.getPositionX());
        tourDynamics.setPositionY(position.getPositionY());
        return tourDynamics;
    }

    @Override
    public int addCommentNum(Integer tourdynaId) {
        return tourDynamicsMapper.updateCommentNum(tourdynaId);
    }

    @Override
    public List<TourDynamics> getTuiJie(Integer type, String placeTo, Date tourdynaOutTime) {
        return tourDynamicsMapper.getTuiJie(type,placeTo,tourdynaOutTime);
    }

    @Override
    public List<TourDynamics> showNearestNews() {
        List<TourDynamics> dynamicsList=tourDynamicsMapper.showNearestNews();
        Position position;
        for (TourDynamics tourDynamics:dynamicsList){
            position=positionService.showPosition(tourDynamics.getTourdynaId());
            tourDynamics.setPositionX(position.getPositionX());
            tourDynamics.setPositionY(position.getPositionY());
        }
        return dynamicsList;
    }
}
