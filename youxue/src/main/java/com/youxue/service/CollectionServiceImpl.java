package com.youxue.service;

import com.youxue.dao.CollectionMapper;
import com.youxue.entity.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
@Service(value = "collectionService")
public class CollectionServiceImpl implements CollectionService{

    @Autowired
    CollectionMapper collectionMapper;
    @Override
    public int addACollection(Collection collection) {
        return collectionMapper.insertSelective(collection);
    }

    @Override
    public int reduceACollection(Integer collId) {
        return collectionMapper.deleteByPrimaryKey(collId);
    }

    @Override
    public Collection showACollection(Integer userId, Integer dnyId) {
        return collectionMapper.selectACollection(userId,dnyId);
    }

    @Override
    public List<Collection> showAllCollectionByPage(Integer userId) {
        return collectionMapper.selectAllColl(userId);
    }

    @Override
    public int deleteCollection(Integer tourId) {
        return collectionMapper.deleteCollection(tourId);
    }
}
