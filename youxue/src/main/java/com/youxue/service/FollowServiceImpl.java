package com.youxue.service;

import com.youxue.dao.FollowMapper;
import com.youxue.entity.Follow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
@Service(value = "FollowService")
public class FollowServiceImpl implements FollowService {

    @Autowired
    FollowMapper followMapper;

    @Override
    public int addFollow(Follow follow) {
        return followMapper.insertSelective(follow);
    }

    @Override
    public List<Follow> showAllFollowF(Integer userIdF) {
        return followMapper.selectFollows(userIdF);
    }



    @Override
    public List<Follow> showAllFollowT(Integer userIdT) {
        return followMapper.selectFuns(userIdT);
    }

    @Override
    public int reduceFollow(Integer userIdF, Integer userIdT) {
        return followMapper.deleteAFollow(userIdF,userIdT);
    }

    @Override
    public Follow showAFollow(Integer userIdF, Integer userIdT) {
        return followMapper.selectAFollow(userIdF,userIdT);
    }
}
