package com.youxue.service;

import com.youxue.entity.Collection;

import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
public interface CollectionService {
    /**
     * 添加一条收藏
     * @param collection
     * @return
     */
    int addACollection(Collection collection);

    /**
     * 删除一条收藏
     * @param collId
     * @return
     */
    int reduceACollection(Integer collId);

    /**
     * 查询该用户是否已经收藏该动态
     * @param userId
     * @param dnyId
     * @return
     */
    Collection showACollection(Integer userId,Integer dnyId);

    /**
     * 展示一个用户所有的收藏记录
     * @param userId
     * @return
     */
    List<Collection> showAllCollectionByPage(Integer userId);

    /**
     * 通过游记id删除用户收藏记录
     * @param tourId
     * @return
     */
    int deleteCollection(Integer tourId);
}
