package com.youxue.service;

import com.youxue.entity.Comment;

import java.util.List;

/**
 * Created by 15c on 2018/5/22.
 */
public interface CommentService {
    /**
     * 统计一条动态的直接评论条数（用于分页统计直接评论数，也用于判断该动态是否有直接评论）
     *
     * @param tourdynamicsId
     * @return
     */
    int countCommentNum(Integer tourdynamicsId);

    /**
     * 添加一条动态直接评论或下方的回复
     *
     * @param comment
     * @return
     */
    int addComment(Comment comment);

    /**
     * 展示一条评论下所有回复信息（分页显示）
     *包括（直接评论用户、直接评论信息、回复者信息、回复内容、被回复者信息）
     * @param commentId
     * @return
     */
    List<Comment> showACommentAllReplys(Integer startPos, Integer pageSize, Integer commentId);

    /**
     * 统计一条直接评论下的所有回复总数（用于分页统计评论回复数）
     * @param commentId
     * @return
     */
    int countACommentAllReplysNum(Integer commentId);

    /**
     * 展示一条评论、评论者信息（用于没有回复时）
     * @param commentId
     * @return
     */
    Comment showCommentAndUserInfo(Integer commentId);

    /**
     * 展示一个用户为读信息
     * @param userId
     * @return
     */
    List<Comment> showAUserNoRead(Integer userId);

    /**
     * 设置消息已读
     * @param commentId
     * @return
     */
    int setUpYesRead(Integer commentId);

    /**
     * 通过动态id删除评论以及回复
     * @param tourId
     */
    int deleteComment(Integer tourId);
}
