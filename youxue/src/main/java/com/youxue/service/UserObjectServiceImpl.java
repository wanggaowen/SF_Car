package com.youxue.service;


import com.youxue.dao.UserObjectMapper;
import com.youxue.entity.Position;
import com.youxue.entity.TourDynamics;
import com.youxue.entity.UserObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 15c on 2018/4/23.
 */
@Service(value = "userObjectService")
public class UserObjectServiceImpl implements UserObjectService {
    @Autowired
    private UserObjectMapper userObjectMapper;

    @Override
    public int addUser(UserObject user) {
        return userObjectMapper.insertSelective(user);
    }

    /*
       * 这个方法中用到了我们开头配置依赖的分页插件pagehelper
       * 很简单，只需要在service层传入参数，然后将参数传递给一个插件的一个静态方法即可；
       * pageNum 开始页数
       * pageSize 每页显示的数据条数
       * */
    @Override
    public List<UserObject> findAllUser() {

        return userObjectMapper.selectAllUser();
    }

    /**
     * 通过telPhone查找用户的userId
     *
     * @param telphone return user对象
     */
    public UserObject selectUserIdByTelphone(UserObject telphone) {
        return userObjectMapper.selectIdByTelPhone(telphone);
    }

    /**
     * 通过userEmail查找用户的userId
     *
     * @param userEmail return user对象
     */
    public UserObject selectUserIdByuserEmail(UserObject userEmail) {
        return userObjectMapper.selectIdByuserEmail(userEmail);
    }

    /**
     * 通过用户id获取其所有信息
     *
     * @param userId
     * @return
     */
    public UserObject selectAllUserInfoByUserId(Integer userId) {
        return userObjectMapper.selectByPrimaryKey(userId);
    }

    /**
     * 通过邮箱查询用户
     *
     * @param email
     * @return
     */
    @Override
    public UserObject showUserByEmail(String email) {
        return userObjectMapper.selectUserByEmail(email);
    }

    /**
     * 通过用户phone查找用户id
     *
     * @param phone
     * @return
     */
    @Override
    public UserObject selectUserIdByPhone(UserObject phone) {
        return userObjectMapper.selectUserIdByPhone(phone);
    }

    /**
     * 用户个人信息展示
     *
     * @param userId
     * @return
     */
    @Override
    public UserObject showUserPartInfo(Integer userId) {
        return userObjectMapper.selectAUserPartInfo(userId);
    }

    /**
     * 用户修改个人资料
     *
     * @param user
     * @return
     */
    @Override
    public int updateUser(UserObject user) {
        return userObjectMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * 通过用户email查找用户id
     *
     * @param email
     * @return
     */
    @Override
    public UserObject selectUserIdByEmail(UserObject email) {
        return userObjectMapper.selectUserIdByEmail(email);
    }

    @Autowired PositionService positionService;
    /**
     * 通过邮箱重置密码
     * @param user
     * @return
     */
    @Override
    public int resetPasswordByemail(UserObject user) {
        return userObjectMapper.updatePasswordByemail(user);
    }

    /**
     * 查询自己主页或他人主页（包括个人信息、动态列表信息、关注数、fans数）
     *
     * @param userId
     * @return
     */
    @Override
    public UserObject showToursByMyAndOther(Integer userId) {
        UserObject userObject=userObjectMapper.selectToursByMyAndOther(userId);
        try {
            //此处可能会为空指针异常
            List<TourDynamics> dynamicsList=userObject.getDynamicsList();
            Position position;
            for(TourDynamics tourDynamics:dynamicsList){
                position=positionService.showPosition(tourDynamics.getTourdynaId());
                tourDynamics.setPositionX(position.getPositionX());
                tourDynamics.setPositionY(position.getPositionY());
            }
            userObject.setDynamicsList(dynamicsList);
            return userObject;
        }catch (NullPointerException e){
            return userObject;
        }
    }

    @Override
    public Boolean saveVerifyInfo(String eamil,String verify_realName, String verify_id, String id_photo, String living_photo, String driving_license_photo, String license_plate_photo) {
        return true;
    }
}
