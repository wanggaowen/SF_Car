package com.youxue.dao;

import com.youxue.entity.Follow;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FollowMapper {
    int deleteByPrimaryKey(Integer followId);

    int insert(Follow record);

    int insertSelective(Follow record);

    Follow selectByPrimaryKey(Integer followId);

    int updateByPrimaryKeySelective(Follow record);

    int updateByPrimaryKey(Follow record);

    Follow selectAFollow(@Param(value = "userFId") Integer userFId,@Param(value = "userTId") Integer userTId);

    List<Follow> selectFuns(@Param(value = "userIdT") Integer userIdT);

    /**
     * 查询关注的用户
     * @param userIdF
     * @return
     */
    List<Follow> selectFollows(@Param(value = "userIdF")Integer userIdF);

    /**
     * 取消关注
     * @param userIdF
     * @param userIdT
     * @return
     */
    int deleteAFollow(@Param(value = "userIdF")Integer userIdF,@Param(value = "userIdT") Integer userIdT);
}