package com.youxue.dao;

import com.youxue.entity.Feedback;

public interface FeedbackMapper {
    int deleteByPrimaryKey(Integer feedId);

    int insert(Feedback record);

    int insertSelective(Feedback record);

    Feedback selectByPrimaryKey(Integer feedId);

    int updateByPrimaryKeySelective(Feedback record);

    int updateByPrimaryKey(Feedback record);
}