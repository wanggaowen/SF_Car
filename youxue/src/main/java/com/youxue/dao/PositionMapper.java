package com.youxue.dao;

import com.youxue.entity.Position;

public interface PositionMapper {
    int deleteByPrimaryKey(Integer tourpoId);

    int insert(Position record);

    int insertSelective(Position record);

    Position selectByPrimaryKey(Integer tourpoId);

    int updateByPrimaryKeySelective(Position record);

    int updateByPrimaryKey(Position record);
}