package com.youxue.dao;

import com.youxue.entity.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentMapper {
    int deleteByPrimaryKey(Integer commentId);

    int insert(Comment record);

    int insertSelective(Comment record);

    Comment selectByPrimaryKey(Integer commentId);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);

    /**
     * 统计一条动态的直接评论条数
     *
     * @param tourdynamicsId
     * @return
     */
    int countCommentNum(@Param(value = "tourdynamicsId") Integer tourdynamicsId);


    int selectACommentAllReplysNum(@Param(value = "commentId") Integer commentId);

    Comment selectCommentAndUserInfo(@Param(value = "commentId") Integer commentId);

    List<Comment> selectACommentAllReplysByPage(@Param(value = "startPos") Integer startPos, @Param(value = "pageSize") Integer pageSize, @Param(value = "commentId") Integer commentId);

    /**
     * 查询未读信息列表
     *
     * @param userId
     * @return
     */
    List<Comment> selectAUserNoRead(@Param(value = "userId") Integer userId);


    int updateYesRead(@Param(value = "commentId") Integer commentId);


    int deleteComment(@Param(value = "tourId") Integer tourId);
}