package com.youxue.dao;

import com.youxue.entity.Collection;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectionMapper {
    int deleteByPrimaryKey(Integer collId);

    int insert(Collection record);

    int insertSelective(Collection record);

    Collection selectByPrimaryKey(Integer collId);

    int updateByPrimaryKeySelective(Collection record);

    int updateByPrimaryKey(Collection record);

    //自己加的
    Collection selectACollection(@Param(value = "userId") Integer userId, @Param(value = "dnyId") Integer dnyId);

    List<Collection> selectAllColl(@Param(value = "userId") Integer userId);

    int deleteCollection(@Param(value = "tourId")Integer tourId);
}