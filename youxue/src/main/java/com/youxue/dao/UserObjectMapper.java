package com.youxue.dao;

import com.youxue.entity.UserObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserObjectMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(UserObject record);

    int insertSelective(UserObject record);

    UserObject selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(UserObject record);

    int updateByPrimaryKey(UserObject record);
    //这个方式我自己加的
    List<UserObject> selectAllUser();

    UserObject selectIdByTelPhone(UserObject telPhone);

    UserObject selectIdByuserEmail(UserObject userEmail);

    UserObject selectUserByEmail(@Param(value = "email") String email);

    UserObject selectUserIdByPhone(UserObject user);

    UserObject selectAUserPartInfo(Integer userId);

    UserObject selectUserIdByEmail(UserObject user);

    int updatePasswordByemail(UserObject user);

    UserObject selectToursByMyAndOther(Integer userId);
}