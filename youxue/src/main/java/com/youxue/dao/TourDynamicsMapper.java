package com.youxue.dao;

import com.youxue.entity.TourDynamics;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface TourDynamicsMapper {
    int deleteByPrimaryKey(Integer tourdynaId);

    int insert(TourDynamics record);

    int insertSelective(TourDynamics record);

    /**
     * 查询一个动态详情（包括用户信息）
     *
     * @param tourdynaId
     * @return
     */
    TourDynamics selectByPrimaryKey(@Param(value = "tourdynaId") Integer tourdynaId);

    int updateByPrimaryKeySelective(TourDynamics record);

    int updateByPrimaryKey(TourDynamics record);

    int selectTourDynamicsId(@Param(value = "userId") Integer userId);

    List<TourDynamics> selectAllTourDynamics(@Param(value = "startPos") Integer startPos, @Param(value = "pageSize") Integer pageSize);

    int selectAllTourDynamicsNum();

    List<TourDynamics> selectAllTourDynamicsByFollow(@Param(value = "startPos") Integer startPos, @Param(value = "pageSize") Integer pageSize, @Param(value = "userId") Integer userId);

    int selectAllTourDynamicsFollowNum(@Param(value = "userId") Integer userId);

    List<TourDynamics> selectAllTourDynamicsByNew(@Param(value = "startPos") Integer startPos, @Param(value = "pageSize") Integer pageSize);

    List<TourDynamics> selectTourDynamicsBySearch(@Param(value = "startPos") Integer startPos, @Param(value = "pageSize") Integer pageSize, @Param(value = "keyword") String keyword, @Param(value = "type") Integer type);

    int selectTourDynamicsBySearchNum(@Param(value = "keyword") String keyword, @Param(value = "type") Integer type);

    int updateLikeNum(@Param(value = "tourdynaId") Integer tourdynaId);

    int reduceLikeNum(@Param(value = "tourdynaId") Integer tourdynaId);

    //没用
    List<TourDynamics> selectToursByMyAndOther(@Param(value = "userId") Integer userId);

    /**
     * 查询一条动态下所有直接评论信息
     *
     * @param tourdynaId
     * @return
     */
    TourDynamics selectAToursAllComment(@Param(value = "startPos") Integer startPos, @Param(value = "pageSize") Integer pageSize, @Param(value = "tourdynaId") Integer tourdynaId);


    int updateCommentNum(@Param(value = "tourdynaId") Integer tourdynaId);

    List<TourDynamics> getTuiJie(@Param(value = "type") Integer type, @Param(value = "placeTo") String placeTo, @Param(value = "tourdynaOutTime") Date tourdynaOutTime);

    List<TourDynamics> showNearestNews();
}