package com.youxue.dao;

import com.youxue.entity.Likes;
import com.youxue.entity.LikesKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LikesMapper {
    int deleteByPrimaryKey(LikesKey key);

    int insert(Likes record);

    int insertSelective(Likes record);

    Likes selectByPrimaryKey(LikesKey key);

    int updateByPrimaryKeySelective(Likes record);

    int updateByPrimaryKey(Likes record);

    int deleteLikes(@Param(value = "tourdynaId")Integer tourdynaId);

    List<Likes> selectAllLikesByuserId(@Param(value = "userId")Integer userId);
}