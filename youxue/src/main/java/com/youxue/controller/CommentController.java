package com.youxue.controller;

import com.alibaba.fastjson.JSONObject;
import com.cangshi.permission.entity.UserSubject;
import com.youxue.entity.Comment;
import com.youxue.entity.CorrectJsonObject;
import com.youxue.entity.ErrorJsonObject;
import com.youxue.entity.UserEntity;
import com.youxue.permission.saver.SubjectHolder;
import com.youxue.service.CommentService;
import com.youxue.service.TourDynamicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 15c on 2018/5/23.
 */
@Controller
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    CommentService commentService;

    @Autowired
    TourDynamicsService tourDynamicsService;

    /**
     * 添加评论或回复
     * 直接评论时：parentId：0；replayUserid：动态发布者id；
     * 添加回复：parentId——commentId；
     *
     * @param comment
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    String add(@RequestBody(required = false) Comment comment) {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == comment) {
            errorJsonObject = new ErrorJsonObject(101, "comment对象为空");
            return JSONObject.toJSONString(errorJsonObject);
        }
        if (null == comment.getTourdynamicsId() || null == comment.getParentId() || null == comment.getReplayUserid() || null == comment.getCommentContent()) {
            errorJsonObject = new ErrorJsonObject(102, "comment对象必要属性不完整");
            return JSONObject.toJSONString(errorJsonObject);
        }
        // 获取当前访问对象
        UserSubject subject = (UserSubject) SubjectHolder.getSubject();
        //获取当前的登录信息
        UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
        //获取当前的用户登录id
        int userId = userEntity.getUserId();
        comment.setCommentUserid(userId);
        int result = commentService.addComment(comment);
        if (1 == result) {
            //为直接评论时
            if (0 == comment.getParentId()) {
                //修改动态评论数
                tourDynamicsService.addCommentNum(comment.getTourdynamicsId());
            }
            correctJsonObject = new CorrectJsonObject("评论成功");
            return JSONObject.toJSONString(correctJsonObject);
        } else {
            errorJsonObject = new ErrorJsonObject(103, "评论失败，请重试");
            return JSONObject.toJSONString(errorJsonObject);
        }
    }

    /**
     * 展示未读消息
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/show/new", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String showNew() {
        // 获取当前访问对象
        UserSubject subject = (UserSubject) SubjectHolder.getSubject();
        //获取当前的登录信息
        UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
        //获取当前的用户登录id
        int userId = userEntity.getUserId();
        List<Comment> commentList = commentService.showAUserNoRead(userId);
        CorrectJsonObject correctJsonObject = new CorrectJsonObject("查看成功");
        correctJsonObject.setData(commentList);
        return JSONObject.toJSONString(correctJsonObject);
    }

    /**
     * 忽略未读消息，
     * @param commentIds
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/read/new", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String readNew(@RequestParam(required = false) String commentIds){
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == commentIds) {
            errorJsonObject = new ErrorJsonObject(106, "参数不完整");
            return JSONObject.toJSONString(errorJsonObject);
        }
        if ("".equals(commentIds)) {
            errorJsonObject = new ErrorJsonObject(107, "commentIds为空");
            return JSONObject.toJSONString(errorJsonObject);
        }
        //以‘,’分割scIds
        String[] commentId;
        commentId = commentIds.split(",");
        int intId;
        int result;
        int allResult = 0;
        String s = "";
        for (String id : commentId) {
            intId = Integer.parseInt(id);//[String]待转换的字符串
            result = commentService.setUpYesRead(intId);
            allResult += result;
        }
        if (allResult == commentId.length) {
            correctJsonObject = new CorrectJsonObject("忽略成功");
            return JSONObject.toJSONString(correctJsonObject);
        } else {
            errorJsonObject = new ErrorJsonObject(108, (commentId.length - allResult) + "个忽略失败");
            return JSONObject.toJSONString(errorJsonObject);
        }
    }

}
