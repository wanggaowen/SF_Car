package com.youxue.controller;

import com.alibaba.fastjson.JSONObject;
import com.cangshi.permission.entity.UserSubject;
import com.youxue.entity.*;
import com.youxue.permission.saver.SubjectHolder;
import com.youxue.service.CollectionService;
import com.youxue.service.TourDynamicsService;
import com.youxue.service.UserObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
@Controller
@RequestMapping("/collection")
public class CollectionController {
    @Autowired
    CollectionService collectionService;

    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String add(@RequestParam(required = false) Integer tourdynaId) {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == tourdynaId) {
            errorJsonObject = new ErrorJsonObject(101, "收藏tourdynaId为null");
            return JSONObject.toJSONString(errorJsonObject);
        }
        // 获取当前访问对象
        UserSubject subject = (UserSubject) SubjectHolder.getSubject();
        //获取当前的登录信息
        UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
        //获取当前的用户登录id
        int userId = userEntity.getUserId();

        Collection resCollection = collectionService.showACollection(userId, tourdynaId);
        if (null != resCollection) {
            errorJsonObject = new ErrorJsonObject(104, "该动态已经添加收藏，请勿重复添加");
            return JSONObject.toJSONString(errorJsonObject);
        } else {
            Collection collection=new Collection();
            collection.setCollUserid(userId);
            collection.setCollDynaid(tourdynaId);
            int result = collectionService.addACollection(collection);
            if (1 == result) {
                correctJsonObject = new CorrectJsonObject("收藏成功");
                return JSONObject.toJSONString(correctJsonObject);
            } else {
                errorJsonObject = new ErrorJsonObject(103, "收藏失败，请重试");
                return JSONObject.toJSONString(errorJsonObject);
            }
        }
    }

    /**
     * 实现一条或多条动态取消收藏
     *
     * @param collIds 多id字符拼接，以‘,’相隔
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String deleteSCs(@RequestParam(required = false) String collIds) {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == collIds) {
            errorJsonObject = new ErrorJsonObject(106, "参数不完整");
            return JSONObject.toJSONString(errorJsonObject);
        }
        if ("".equals(collIds)) {
            errorJsonObject = new ErrorJsonObject(107, "collIds为空");
            return JSONObject.toJSONString(errorJsonObject);
        }
        //以‘,’分割scIds
        String[] collId;
        collId = collIds.split(",");
        int intId;
        int result;
        int allResult = 0;
        String s = "";
        for (String id : collId) {
            intId = Integer.parseInt(id);//[String]待转换的字符串
            result = collectionService.reduceACollection(intId);
            allResult += result;
        }
        if (allResult == collId.length) {
            correctJsonObject = new CorrectJsonObject("删除成功");
            return JSONObject.toJSONString(correctJsonObject);
        } else {
            errorJsonObject = new ErrorJsonObject(108, (collId.length - allResult) + "个删除失败");
            return JSONObject.toJSONString(errorJsonObject);
        }
    }

    @Autowired
    TourDynamicsService tourDynamicsService;
    @Autowired
    UserObjectService userObjectService;

    /**
     * 获取当前登录用户所有收藏
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/show", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String showColls() {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        // 获取当前访问对象
        UserSubject subject = (UserSubject) SubjectHolder.getSubject();

        //获取当前的登录信息
        UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
        //获取当前的用户登录id
        int userId = userEntity.getUserId();

        List<Collection> collectionList = collectionService.showAllCollectionByPage(userId);
        //添加动态发布者信息
        for (Collection collection : collectionList) {
            //查询收藏动态详情
            TourDynamics tourDynamics = tourDynamicsService.showAToursInfo(collection.getCollDynaid());
            //设置动态发布者信息
            tourDynamics.setUser(userObjectService.showUserPartInfo(tourDynamics.getTourdynaUserId()));
            collection.setTourDynamics(tourDynamics);
        }
        if (0 == collectionList.size()) {
            errorJsonObject = new ErrorJsonObject(105, "暂未收藏或添加任何动态");
            return JSONObject.toJSONString(errorJsonObject);
        }
        correctJsonObject = new CorrectJsonObject("查询成功");
        correctJsonObject.setData(collectionList);
        return JSONObject.toJSONString(correctJsonObject);
    }
}
