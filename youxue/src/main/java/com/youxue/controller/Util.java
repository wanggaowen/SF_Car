package com.youxue.controller;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 15c on 2019/5/30.
 */
public class Util {
    static Map savePhoto(HttpServletRequest request,
                         String id_photo,
                         String living_photo,
                         String driving_license_photo,
                         String license_plate_photo) {
        Map hashMap = new HashMap();
        hashMap.put("id_photo", id_photo);
        hashMap.put("living_photo", living_photo);
        hashMap.put("driving_license_photo", driving_license_photo);
        hashMap.put("license_plate_photo", license_plate_photo);
        return hashMap;
    }
}
