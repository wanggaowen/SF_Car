package com.youxue.controller;

import com.alibaba.fastjson.JSONObject;
import com.cangshi.permission.entity.UserSubject;
import com.youxue.entity.CorrectJsonObject;
import com.youxue.entity.ErrorJsonObject;
import com.youxue.entity.Follow;
import com.youxue.entity.UserEntity;
import com.youxue.permission.saver.SubjectHolder;
import com.youxue.service.FollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by 15c on 2018/5/21.
 */
@Controller
@RequestMapping("/follow")
public class FollowController {
    @Autowired
    FollowService followService;

    /**
     * 添加关注
     *
     * @param UserId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String addFollow(@RequestParam(required = false) Integer UserId) {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == UserId) {
            errorJsonObject = new ErrorJsonObject(101, "userId为空");
            return JSONObject.toJSONString(errorJsonObject);
        }
        // 获取当前访问对象
        UserSubject subject = (UserSubject) SubjectHolder.getSubject();
        //获取当前的登录信息
        UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
        //获取当前的用户登录id
        int userId = userEntity.getUserId();
        if (userId == UserId) {
            errorJsonObject = new ErrorJsonObject(102, "不可关注自己哟");
            return JSONObject.toJSONString(errorJsonObject);
        }
        //查询是否已经关注
        Follow resulFollow = followService.showAFollow(userId, UserId);
        if (null != resulFollow) {
            errorJsonObject = new ErrorJsonObject(104, "请勿重复关注");
            return JSONObject.toJSONString(errorJsonObject);
        } else {
            Follow follow=new Follow();
            follow.setFollowfUserid(userId);
            follow.setFollowtUserid(UserId);
            int result = followService.addFollow(follow);
            if (1 == result) {
                correctJsonObject = new CorrectJsonObject("关注成功");
                return JSONObject.toJSONString(correctJsonObject);
            } else {
                errorJsonObject = new ErrorJsonObject(103, "关注失败，请重试");
                return JSONObject.toJSONString(errorJsonObject);
            }
        }

    }

    /**
     * 取消关注
     *
     * @param userTId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String reduceFollows(@RequestParam(required = false) Integer userTId) {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == userTId) {
            errorJsonObject = new ErrorJsonObject(106, "参数不完整");
            return JSONObject.toJSONString(errorJsonObject);
        }
        // 获取当前访问对象
        UserSubject subject = (UserSubject) SubjectHolder.getSubject();
        //获取当前的登录信息
        UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
        //获取当前的用户登录id
        int userId = userEntity.getUserId();
        //查询是否已经关注
        Follow resulFollow = followService.showAFollow(userId, userTId);
        if (null == resulFollow) {
            errorJsonObject = new ErrorJsonObject(107, "你还未关注他");
            return JSONObject.toJSONString(errorJsonObject);
        } else {


            int result = followService.reduceFollow(userId, userTId);
            if (1 == result) {
                correctJsonObject = new CorrectJsonObject("取关成功");
                return JSONObject.toJSONString(correctJsonObject);
            } else {
                errorJsonObject = new ErrorJsonObject(108, "取关失败，请重试");
                return JSONObject.toJSONString(errorJsonObject);
            }
        }
    }

    /**
     * 查询粉丝列表(查询自己的以及别人的)
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/showFans", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String showFans(@RequestParam(required = false) Integer userId) {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == userId) {
            // 获取当前访问对象
            UserSubject subject = (UserSubject) SubjectHolder.getSubject();
            //获取当前的登录信息
            UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
            //获取当前的用户登录id
            userId = userEntity.getUserId();
        }
        List<Follow> followList = followService.showAllFollowT(userId);
        if (0 == followList.size()) {
            errorJsonObject = new ErrorJsonObject(109, "粉丝正在赶来");
            return JSONObject.toJSONString(errorJsonObject);
        }
        correctJsonObject = new CorrectJsonObject("查询成功");
        correctJsonObject.setData(followList);
        //fans数量
        correctJsonObject.put("allNum", followList.size());
        return JSONObject.toJSONString(correctJsonObject);
    }


    /**
     * 查询关注用户(查询自己的以及别人的)
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/showFollows", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String showFollowUsers(@RequestParam(required = false) Integer userId) {
        ErrorJsonObject errorJsonObject = null;
        CorrectJsonObject correctJsonObject = null;
        if (null == userId) {
            // 获取当前访问对象
            UserSubject subject = (UserSubject) SubjectHolder.getSubject();
            //获取当前的登录信息
            UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
            //获取当前的用户登录id
            userId = userEntity.getUserId();
        }
        List<Follow> followList = followService.showAllFollowF(userId);
        if (0 == followList.size()) {
            errorJsonObject = new ErrorJsonObject(110, "还在寻觅知音");
            return JSONObject.toJSONString(errorJsonObject);
        }
        correctJsonObject = new CorrectJsonObject("查询成功");
        correctJsonObject.setData(followList);
        //关注数
        correctJsonObject.put("allNum", followList.size());
        return JSONObject.toJSONString(correctJsonObject);

    }

    /**
     * 查询是否已关注指定用户
     *
     * @param userId 指定用户id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/booleanFollows", method = RequestMethod.GET, produces = "text/html;charset=UTF-8")
    String booleanFollows(@RequestParam(required = false) Integer userId) {
        CorrectJsonObject correctJsonObject = null;
        ErrorJsonObject errorJsonObject = null;
        // 获取当前访问对象
        UserSubject subject = (UserSubject) SubjectHolder.getSubject();
        //获取当前的登录信息
        UserEntity userEntity = (UserEntity) subject.getAuthorizationEntity();
        //获取当前的用户登录id
        Integer uId = userEntity.getUserId();
        Follow follow = followService.showAFollow(uId, userId);
        if (null == follow) {
            correctJsonObject = new CorrectJsonObject("还未关注");
            correctJsonObject.setData(null);
            return JSONObject.toJSONString(correctJsonObject);
        } else {
            errorJsonObject = new ErrorJsonObject(1, "还未关注");
            return JSONObject.toJSONString(errorJsonObject);
        }
    }

}
